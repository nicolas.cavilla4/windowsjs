
const mouseEvent = {
    click: false,
    drag: false,
    offsetX: 0,
    offsetY: 0,
    target: null,
    inicio: false
}

let carpetasSeleccionadas = [];

const reloj = document.querySelector('#hora');
const inicio = document.querySelector('#inicio');
const menu = document.querySelector('#menu-content');



const calcularHora = ()=>{
    let hora = new Date().getHours();
    let minuto = new Date().getMinutes();
    const p = hora >= 12 ? 'PM' : 'AM';
    hora = hora > 12 ? hora - 12 : hora;
    hora = hora.toString().length < 2 ? '0'+hora : hora;
    minuto = minuto.toString().length < 2 ? '0'+minuto : minuto;

    reloj.innerHTML = hora+':'+minuto+' '+p;
}
setInterval(calcularHora, 1000)

inicio.addEventListener('mousedown', function(e){
    if(!mouseEvent.inicio){
        mouseEvent.inicio = true;
        inicio.style.borderColor = "black white white black";
        menu.style.display = 'block';
    }else{
        desactivarMenu();
    }
});

function desactivarMenu(){
    inicio.style.borderColor = "white black black white";
    mouseEvent.inicio = false;
    menu.style.display = 'none';
}

function deseleccionarCarpetas(){
    carpetasSeleccionadas.map(carpeta=>{
        child.style.background = "rgba(0,0,255,0)";
        child.style.border = "solid 1px rgba(0,0,0,0)";
    })
    carpetasSeleccionadas = [];
}

function seleccionarCarpeta(carpeta){
    carpetasSeleccionadas.push(carpeta);
    child = carpeta.childNodes[3];
    child.style.background = "rgba(0,0,255,0.5)";
    child.style.border = "dotted 1px #fff";
}

document.addEventListener('mousedown', function(e){
    mouseEvent.offsetX = e.clientX;
    mouseEvent.offsetY = e.clientY;

    mouseEvent.click = true;
    mouseEvent.target = e.target;

    setTimeout(()=>{
        mouseEvent.click ? mouseEvent.drag = true : null;
    },30);

    if(e.target.id != "inicio"){
        // Cerrar Menu de inicio y restablecer el boton
        desactivarMenu();
    }

    const parent = e.target.parentNode
    deseleccionarCarpetas();
    if(parent.classList && parent.classList.contains('carpeta')){
        seleccionarCarpeta(parent);
        mouseEvent.target = parent;
    }
})

document.addEventListener('mouseup', function(e){
    mouseEvent.drag = false;
    mouseEvent.click = false;
    mouseEvent.target = null;
    mouseEvent.offsetX = 0;
    mouseEvent.offsetY = 0;
})

document.addEventListener('mousemove', function(e){
    if(mouseEvent.drag){
        const target = mouseEvent.target;
        const {x,y} = target.getBoundingClientRect();
        target.style.top = (y + e.clientY - mouseEvent.offsetY)+'px';
        target.style.left = (x + e.clientX - mouseEvent.offsetX)+'px';
        mouseEvent.offsetX = e.clientX;
        mouseEvent.offsetY = e.clientY;
    }
})